const { 
    tb_usuario, 
    tb_corpo_clinico, 
    tb_unidade_atendimento, 
    tb_especialidade_corpo_clinico, 
    tb_profissao 
} = require('../models');
const jwt  = require('jsonwebtoken')
const authConfig = require('../../config/auth.json');
const crypto = require('crypto')

module.exports = {

    async index(req, res) {
        const { username, password } = req.body;
        await tb_corpo_clinico
            .findOne({
                where: {
                    situacao: "A"
                },
                include: [
                    {
                        model: tb_usuario,
                        where: {
                            login: username,
                            situacao: "A"
                        }
                    },{
                        model: tb_unidade_atendimento,
                        where: {
                            situacao: "A"
                        }
                    }
                ],
            })
            .then((staff) => {
             if(!staff){
                  return  res.status(401).send({success: false, msg: 'Usuário inválido.'});
                }
              
                const hash = crypto.createHash('sha256').update(password).digest('hex');
                staff.tb_usuario.comparePassword(hash, (err, isMatch) => {                 
                    
                  if(isMatch && !err) {		                      
                        tb_especialidade_corpo_clinico
                        .findOne({
                            where: {
                                id_corpo_clinico: staff.id
                            },
                            include: [
                                {
                                    model: tb_profissao
                                }
                            ]
                        }) 
                        .then((speciality) => {
                            if (!speciality) {
                                return res.status(401).send({success: false, msg: 'O usuário não possui uma profissão cadastrada.'});
                            }
                            
                            const token = jwt.sign(JSON.parse(JSON.stringify(staff.tb_usuario)), authConfig.secret, {
                                expiresIn: 3600 * 2,
                            });
                            
                            const user = {
                                id: staff.tb_usuario.id,
                                nome: staff.tb_usuario.nome,
                                nr_cpf: staff.tb_usuario.nr_cpf,
                                cnes: staff.tb_unidade_atendimento.cod_cnes,
                                unidade_atendimento_id: staff.tb_unidade_atendimento.id,
                                cbo_profissional: speciality.tb_profissao.cod_cbo,
                                equipe_id: parseInt(staff.id_equipe),

                            };

                            return res.json({user, token})
                        })
                        .catch((error) => {
                           return res.status(400).send(error)
                        }) 
                    } else {
                       return res.status(400).send({success: false, msg: 'Invalid password.'});
                    } 
                });                
            })
            .catch((error) => {
                res.status(400).send(error);
            } 
        );
    },

    async show(req, res) {
        const { id_agent } = req.query;

        await tb_corpo_clinico
            .findOne({
                where: {
                    situacao: "A"
                },
                include: [
                    {
                        model: tb_usuario,
                        where: {
                            id: id_agent,
                            situacao: "A"
                        }
                    },{
                        model: tb_unidade_atendimento,
                        where: {
                            situacao: "A"
                        }
                    }
                ],
            })
            .then((staff) => {
                if(!staff){
                    return  res.status(401).send({success: false, msg: 'Usuário inválido.'});
                }	                      
                tb_especialidade_corpo_clinico
                    .findOne({
                        where: {
                            id_corpo_clinico: staff.id
                        },
                        include: [
                            {
                                model: tb_profissao
                            }
                        ]
                    }) 
                    .then((speciality) => {
                        if (!speciality) {
                            return res.status(401).send({success: false, msg: 'O usuário não possui uma profissão cadastrada.'});
                        }                            
                        const userAgent = {
                            id: staff.tb_usuario.id,
                            nome: staff.tb_usuario.nome,
                            nr_cpf: staff.tb_usuario.nr_cpf,
                            cnes: staff.tb_unidade_atendimento.cod_cnes,
                            unidade_atendimento_id: staff.tb_unidade_atendimento.id,
                            cbo_profissional: speciality.tb_profissao.cod_cbo,
                            equipe_id: parseInt(staff.id_equipe),

                        };
                        return res.json({userAgent})
                    })
                    .catch((error) => {
                        return res.status(400).send(error)
                    })                       
            })
            .catch((error) => {
                res.status(400).send(error);
            }); 
    }       
}