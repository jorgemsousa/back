const { tb_unidade_atendimento } = require('../models');


module.exports = {

    async index(req, res) {
        
        try {
            const {count: size, rows: unity } = await tb_unidade_atendimento.findAndCountAll({                
                where: {
                    situacao: "A"
                }             
            })
            return res.json({size, unity})
            
        } catch (error) {
            return error
        }        
    }
}