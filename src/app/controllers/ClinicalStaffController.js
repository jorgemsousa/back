const { tb_corpo_clinico, tb_usuario } = require('../models');


module.exports = {

    async index(req, res) {
        const {id_team, id_unity} = req.query
        try {
            const {count: size, rows: ClinicalStaff } = await tb_corpo_clinico.findAndCountAll({                
                where: {
                    situacao: "A",
                    id_equipe: parseInt(id_team),
                    id_unidade_atendimento: parseInt(id_unity)
                },
                    include: [
                        {
                            model: tb_usuario,
                            where: {
                                situacao: "A"
                            }
                        }
                    ]
                             
            })
            return res.json({size, ClinicalStaff})
            
        } catch (error) {
            return res.status(400).send(error)
        }        
    }
}