const { tb_equipe } = require('../models');


module.exports = {

    async index(req, res) {
        let { id } = req.query
        id = parseInt(id)
        try {
            const {count: size, rows: team } = await tb_equipe.findAndCountAll({                
                where: {
                    id_unidade_atendimento: id
                }             
            })
            return res.json({size, team})
            
        } catch (error) {
            return error
        }        
    }
}