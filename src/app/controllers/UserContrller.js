const { tb_usuario } = require('../models');
const { Sequelize } = require('../models/index')

const Op = Sequelize.Op;

module.exports = {

    async index(req, res) {
        let {limit = 10 } = req.query
        let { page = 1 } = req.query
        page = parseInt(page - 1)
        limit = parseInt(limit)
        try {
            const {count: size, rows: users} = await tb_usuario.findAndCountAll({                
                limit: limit,
                offset: page * limit                
            })
            return res.json({size, users})
            
        } catch (error) {
            return error
        }        
    },

    async show(req, res) {
        try {
            const usuario = await tb_usuario.findOne({ where: { id : req.params._id } })

            return res.send(usuario)
            
        } catch (error) {
            return res.status(400).send({ error: 'Error show register'})
        }
    },
}