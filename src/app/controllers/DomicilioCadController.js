const { tb_cadastro_domiciliar, tb_tipo_logradouro, sequelize } = require('../models');
const { Sequelize } = require('../models/index')
const { QueryTypes } = require('sequelize')
const { v4 : uuidv4 } = require('uuid')
const Op = Sequelize.Op;

module.exports = {

    async index(req, res) {
        const { id } = req.query
        let {limit = 10 } = req.query
        let { page = 1 } = req.query
        page = parseInt(page - 1)
        limit = parseInt(limit)
        try {
            const {count: size, rows: domicilios} = await tb_cadastro_domiciliar.findAndCountAll({
                where: {
                   inactive: {
                       [Op.ne]: true
                   },
                   user_id: id,
                },
                limit: limit,
                offset: page * limit                
            })
            return res.json({size, domicilios})
            
        } catch (error) {
            return res.status(400).send({ error: 'Error list residences'})
        }        
    },
    
    async create(req, res){
        const user = req.body.user
        const data = req.body.data
        const uuid = `${user.cnes}-${uuidv4()}`
        const date = new Date();
      
        const objct = {
            id : uuidv4(),
            user_id : parseInt(user.id),
            uuid : uuid,
            uuid_ficha_originadora : uuid,
            inactive : false,
            dirty : false,
            lat: null,
            lng: null,
            microarea: data.microarea || '',
            st_fora_area: data.st_fora_area || false,
            tipo_imovel: data.tipo_imovel || null,
            numero: data.numero || '',
            complemento: data.complemento.toUpperCase() || '',
            ponto_referencia: data.ponto_referencia.toUpperCase() || '',
            st_sem_numero: data.st_sem_numero || false,
            telelefone_residencia: (data.telelefone_residencia).replace(/[^\d]+/g,'') || '',
            telefone_contato: (data.telefone_contato).replace(/[^\d]+/g,'') || '',
            situacao_moradia_posse_terra: data.situacao_moradia_posse_terra || null,
            localizacao: data.localizacao || null,
            tipo_domicilio: data.tipo_domicilio || null,
            qtd_moradores: data.qtd_moradores || null,
            qtd_comodos: data.qtd_comodos || null,
            tipo_acesso_domicilio: data.tipo_acesso_domicilio || null,
            disponibilidade_energia_eletrica: data.disponibilidade_energia_eletrica || false,
            condicao_posse_uso_terra: data.condicao_posse_uso_terra || null,
            material_predominante_paredes_ext_domicilio: data.material_predominante_paredes_ext_domicilio || null,
            abastecimento_agua: data.abastecimento_agua || null,
            forma_escoamento_banheiro: data.forma_escoamento_banheiro || null,
            agua_consumo_domicilio: data.agua_consumo_domicilio || null,
            destino_lixo: data.destino_lixo || null,
            animais_domicilio: data.animais_domicilio || false,
            cep: data.cep || '',
            cod_ibge_municipio: data.cod_ibge_municipio || '',
            bairro: data.bairro.toUpperCase() || '',
            tipo_logradouro_numero_dne: data.tipo_logradouro_numero_dne || null,
            nome_logradouro: data.nome_logradouro.toUpperCase() || '',
            numero_dneuf: data.numero_dneuf || '',
            nome_instituicao_permanencia: data.nome_instituicao_permanencia ? data.nome_instituicao_permanencia.toUpperCase() : '',
            nome_responsavel_tecnico: data.nome_responsavel_tecnico ? data.nome_responsavel_tecnico.toUpperCase() : '',
            cns_responsavel_tecnico: data.cns_responsavel_tecnico || null,
            cargo_responsavel_tecnico: data.cargo_responsavel_tecnico ? data.cargo_responsavel_tecnico.toUpperCase() : '',
            telefone_responsavel_tecnico: data.telefone_responsavel_tecnico ? (data.telefone_responsavel_tecnico).replace(/[^\d]+/g,'') : '',
            anotacao: data.anotacao.toUpperCase() || '',
            animais_gatos: data.animais_gatos || null,
            animais_caes: data.animais_caes || null,
            animais_passaros: data.animais_passaros || null,
            animais_outros: data.animais_outros || null,
            outros_profissionais_vinculados: data.outros_profissionais_vinculados || false,
            possui_piscina: data.possui_piscina || false,
            date_created: date,
            ficha_atualizada : false,
            status_termo_recusa: data.status_termo_recusa || false,
            unidade_atendimento_id : user.unidade_atendimento_id || null,
            cbo_profissional : user.cbo_profissional || '',
            equipe_id : user.equipe_id || null
        }

        try {
            const domicilio = await tb_cadastro_domiciliar.create(
                objct
            ); 
           return res.send(domicilio)
            
        } catch (error) {
            return res.status(400).send(error)
        } 
    },
    async showAll(req, res) {
        const { id } = req.query
        
        try {
            
            const domicilios = await sequelize.query(
                `SELECT DISTINCT * FROM tb_cadastro_domiciliar as domicilio INNER JOIN tb_tipo_logradouro as logradouro 
                    ON domicilio.tipo_logradouro_numero_dne=logradouro.numero_dne AND user_id=${id}`, { type: QueryTypes.SELECT }
            )

            /*  const {count: size, rows: domicilio} = await tb_cadastro_domiciliar.findAndCountAll({                
                where: {
                    user_id: id
                },
            }) */

            return res.json({domicilios})
            
        } catch (error) {
            return res.status(400).send({ error: 'Error list familys'+ error})
        }        
    },

    async show(req, res) {
        try {
            const domicilio = await tb_cadastro_domiciliar.findOne({ where: { id : req.params._id } })

            return res.send(domicilio)
            
        } catch (error) {
            return res.status(400).send({ error: 'Error show residence'})
        }
    },

    async update(req, res){
        try {
            const domicilio = await tb_cadastro_domiciliar.findOne({ where: { id : req.params._id } })

            return res.send(domicilio)
            
        } catch (error) {
            return res.status(400).send({ error: 'Error update residence'})
        }
    },

    async delete(req, res) {
        try {
            await tb_cadastro_domiciliar.destroy({ where: { id : req.params._id } })

            return res.send()
            
        } catch (error) {
            return res.status(400).send({ error: 'Error delete residence'})
        }
    }
}