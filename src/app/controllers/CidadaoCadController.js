const { tb_cadastro_cidadao } = require('../models');
const { Sequelize } = require('../models/index')

const Op = Sequelize.Op;

module.exports = {

    async index(req, res) {
        const { id } = req.query
        let {limit = 10 } = req.query
        let { page = 1 } = req.query
        page = parseInt(page - 1)
        limit = parseInt(limit)
        try {
            const {count: size, rows: cidadaos} = await tb_cadastro_cidadao.findAndCountAll({
                where: {
                   inactive: {
                       [Op.ne]: true
                   },
                   user_id: id,
                },
                limit: limit,
                offset: page * limit                
            })
            return res.json({size, cidadaos})
            
        } catch (error) {
            return res.status(400).send({ error: 'Error list register'})
        }        
    },

    async showAll(req, res) {
        const { id } = req.query
        
        try {
            const {count: size, rows: cidadaos} = await tb_cadastro_cidadao.findAndCountAll({                
                where: {
                    familia_id: id
                }                
            })
            return res.json({size, cidadaos})
            
        } catch (error) {
            return res.status(400).send({ error: 'Error list familys'})
        }        
    },

    async showResponsavel(req, res) {
        const { id } = req.query
        
        try {
            const {count: size, rows: cidadaos} = await tb_cadastro_cidadao.findAndCountAll({                
                where: {
                    cns_responsavel_familiar: {
                        [Op.ne]: null
                    },
                    user_id: id,
                }                
            })
            return res.json({size, cidadaos})
            
        } catch (error) {
            return res.status(400).send({ error: 'Error list familys'})
        }        
    },    

    async create(req, res){
        try {
            const cidadao = await tb_cadastro_cidadao.create({ ...req.body, user: userId })

            return res.send(cidadao)
            
        } catch (error) {
            return res.status(400).send({ error: 'Error create a new register'})
        }
    },

    async show(req, res) {
        const { id } = req.query
        try {
            const cidadao = await tb_cadastro_cidadao.findOne({ where: { id } })

            return res.send(cidadao)
            
        } catch (error) {
            return res.status(400).send({ error: 'Error show register'})
        }
    },

    async update(req, res){
        try {
            const cidadao = await tb_cadastro_cidadao.findOne({ where: { id : req.params._id } })
            return res.send(cidadao)
            
        } catch (error) {
            return res.status(400).send({ error: 'Error update register'})
        }
    },

    async delete(req, res) {
        try {
            await tb_cadastro_cidadao.destroy({ where: { id : req.params._id } })

            return res.send()
            
        } catch (error) {
            return res.status(400).send({ error: 'Error delete register'})
        }
    }
}