const { tb_cadastro_visita } = require('../models');
const { Sequelize } = require('../models/index')
const { v4 : uuidv4 } = require('uuid')

const Op = Sequelize.Op;

module.exports = {

    async index(req, res) {
        const { id } = req.query
        let {limit = 10 } = req.query
        let { page = 1 } = req.query
        page = parseInt(page - 1)
        limit = parseInt(limit)
        try {
            const {count: size, rows: visitas} = await tb_cadastro_visita.findAndCountAll({               
                limit: limit,
                offset: page * limit,
                where: {
                    user_id: id,
                }                
            })
            return res.json({size, visitas})
            
        } catch (error) {
            return res.status(400).send({ error: 'Error list visits'})
        }        
    },

    async create(req, res){
        const user = req.body.user;
        const data = req.body.data;
        const uuid = `${user.cnes}-${uuidv4()}`
        const date = new Date();
        const dataVisita = new Date(data.data_visita)
        const motivos = data.motivos_visita

        const objctVisit = {
            id: uuidv4(),
            uuid: uuid,
            dirty: false,
            user_id: user.id || null,
            familia_id: data.familia_id || '',
            cadastro_cidadao_uuid: data.cadastro_cidadao_uuid || '',
            cadastro_domiciliar_uuid: data.cadastro_domicilio_uuid || '',
            motivos_visita: motivos.toString() || '',
            desfecho: data.desfecho || null,
            date_created: date,
            date_updated: date,
            anotacao: data.anotacao || '',
            necessita_visita_endemias: data.necessita_visita_endemias || false,
            data_visita: dataVisita || null,
            turno: data.turno || null,
            status_visita_compartilhada_outro_profissional: data.status_visita_compartilhada_outro_profissional || false,
            peso_acompanhamento_nutricional: parseFloat(data.peso_acompanhamento_nutricional) || null,
            altura_acompanhamento_nutricional: parseInt(data.altura_acompanhamento_nutricional) || null,
            unidade_atendimento_id: user.unidade_atendimento_id || null,
            familia_mudou: data.familia_mudou || false,
            saida_cidadao_cadastro: data.saida_cidadao_cadastro || false,
            cbo_profissional: user.cbo_profissional || '',
            equipe_id: user.equipe_id || null,
            problema_exportacao: null,
            id_lote_prod_agente: null,
            exportado: null,
        }
        try {
            const visita = await tb_cadastro_visita.create( objctVisit )
            return res.status(200).send(visita)
            
        } catch (error) {
            return res.status(400).send({ error: 'Error create a new visit' + error})
        }
    },

    async show(req, res) {
        try {
            const visita = await tb_cadastro_visita.findOne({ where: { id : req.params._id } })

            return res.send(visita)
            
        } catch (error) {
            return res.status(400).send({ error: 'Error show visit'})
        }
    },

    async update(req, res){
        try {
            const visita = await tb_cadastro_visita.findOne({ where: { id : req.params._id } })

            return res.send(visita)
            
        } catch (error) {
            return res.status(400).send({ error: 'Error update visit'})
        }
    },

    async delete(req, res) {
        try {
            await tb_cadastro_visita.destroy({ where: { id : req.params._id } })

            return res.send()
            
        } catch (error) {
            return res.status(400).send({ error: 'Error delete visit'})
        }
    }
}