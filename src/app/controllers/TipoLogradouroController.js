const { tb_tipo_logradouro, sequelize } = require('../models');
const { Sequelize } = require('../models/index')

const Op = Sequelize.Op;

module.exports = {

    async index(req, res) {
        try {
            const {count: size, rows: tipoLogradouro} = await tb_tipo_logradouro.findAndCountAll()
            return res.json({size, tipoLogradouro})            
        } catch (error) {
            return error
        }        
    }
}