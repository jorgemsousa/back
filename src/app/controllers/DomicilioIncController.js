const { tb_cadastro_domiciliar } = require('../models');
const { Sequelize } = require('../models/index')

const Op = Sequelize.Op;

module.exports = {

    async index(req, res) {
        let {limit = 10 } = req.query
        let { page = 1 } = req.query
        page = parseInt(page - 1)
        limit = parseInt(limit)
        try {
            const {count: size, rows: domicilios} = await tb_cadastro_domiciliar.findAndCountAll({
                where: {
                   problema_exportacao: {
                       [Op.ne]: null
                   }
                },
                limit: limit,
                offset: page * limit                
            })
            return  res.json({size, domicilios})
            
        } catch (error) {
            return error
        }        
    },

    async show(req, res) {
        try {
            const domicilio = await tb_cadastro_domiciliar.findOne({ where: { id : req.params._id } })

            return res.send(domicilio)
            
        } catch (error) {
            return res.status(400).send({ error: 'Error show residence'})
        }
    },

    async update(req, res){
        try {
            const domicilio = await tb_cadastro_domiciliar.findOne({ where: { id : req.params._id } })

            return res.send(domicilio)
            
        } catch (error) {
            return res.status(400).send({ error: 'Error update residence'})
        }
    },
}
