/* jshint indent: 2 */

const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('tb_tipo_logradouro', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    numero_dne: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    nome: {
      type: DataTypes.STRING(255),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'tb_tipo_logradouro',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "tb_tipo_logradouro_pkey",
        unique: true,
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
};
