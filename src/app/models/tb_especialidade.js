/* jshint indent: 2 */

const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('tb_especialidade', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    nome: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    situacao: {
      type: DataTypes.CHAR(1),
      allowNull: true,
      defaultValue: "A"
    },
    id_usuario_incl: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    data_hora_incl: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: "(now"
    },
    id_usuario_altr: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    data_hora_altr: {
      type: DataTypes.DATE,
      allowNull: true
    },
    recebi_atendimento: {
      type: DataTypes.CHAR(1),
      allowNull: false,
      defaultValue: "S"
    }
  }, {
    sequelize,
    tableName: 'tb_especialidade',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "tb_especialidade_id_pk",
        unique: true,
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
};
