/* jshint indent: 2 */

const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('tb_cadastro_domiciliar', {
    id: {
      type: DataTypes.UUID,
      allowNull: false,
      primaryKey: true
    },
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    uuid: {
      type: DataTypes.STRING(44),
      allowNull: true
    },
    uuid_ficha_originadora: {
      type: DataTypes.STRING(44),
      allowNull: true
    },
    inactive: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
      defaultValue: false
    },
    dirty: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
      defaultValue: false
    },
    lat: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    lng: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    microarea: {
      type: DataTypes.STRING(2),
      allowNull: true
    },
    st_fora_area: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    tipo_imovel: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    numero: {
      type: DataTypes.STRING(10),
      allowNull: true
    },
    complemento: {
      type: DataTypes.STRING(30),
      allowNull: true
    },
    ponto_referencia: {
      type: DataTypes.STRING(40),
      allowNull: true
    },
    st_sem_numero: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    telelefone_residencia: {
      type: DataTypes.STRING(11),
      allowNull: true
    },
    telefone_contato: {
      type: DataTypes.STRING(11),
      allowNull: true
    },
    situacao_moradia_posse_terra: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    localizacao: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    tipo_domicilio: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    qtd_moradores: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    qtd_comodos: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    tipo_acesso_domicilio: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    disponibilidade_energia_eletrica: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    condicao_posse_uso_terra: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    material_predominante_paredes_ext_domicilio: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    abastecimento_agua: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    forma_escoamento_banheiro: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    agua_consumo_domicilio: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    destino_lixo: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    animais_domicilio: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    cep: {
      type: DataTypes.STRING(8),
      allowNull: true
    },
    cod_ibge_municipio: {
      type: DataTypes.STRING(10),
      allowNull: true
    },
    bairro: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    tipo_logradouro_numero_dne: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    nome_logradouro: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    numero_dneuf: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    nome_instituicao_permanencia: {
      type: DataTypes.STRING(40),
      allowNull: true
    },
    nome_responsavel_tecnico: {
      type: DataTypes.STRING(40),
      allowNull: true
    },
    cns_responsavel_tecnico: {
      type: DataTypes.BIGINT,
      allowNull: true
    },
    cargo_responsavel_tecnico: {
      type: DataTypes.STRING(30),
      allowNull: true
    },
    telefone_responsavel_tecnico: {
      type: DataTypes.STRING(11),
      allowNull: true
    },
    anotacao: {
      type: DataTypes.STRING(200),
      allowNull: true
    },
    animais_gatos: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    animais_caes: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    animais_passaros: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    animais_outros: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    outros_profissionais_vinculados: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    possui_piscina: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    date_created: {
      type: DataTypes.DATE,
      allowNull: true
    },
    date_updated: {
      type: DataTypes.DATE,
      allowNull: true
    },
    exportado: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    ficha_atualizada: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    status_termo_recusa: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    unidade_atendimento_id: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    cbo_profissional: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    equipe_id: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    id_lote_prod_agente: {
      type: DataTypes.BIGINT,
      allowNull: true
    },
    problema_exportacao: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    lote_cad_cidadao_gerado: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
      defaultValue: false
    }
  }, {
    sequelize,
    tableName: 'tb_cadastro_domiciliar',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "domicilio_id_lote_prod_agente_idx",
        fields: [
          { name: "id_lote_prod_agente" },
        ]
      },
      {
        name: "domicilio_uuid_ficha_originadora_idx",
        fields: [
          { name: "uuid_ficha_originadora" },
        ]
      },
      {
        name: "tb_cadastro_domiciliar_pk",
        unique: true,
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
};
