/* jshint indent: 2 */

const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('tb_equipe', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true
    },
    id_tipo_equipe: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: 'tb_tipo_equipe',
        key: 'id'
      }
    },
    id_unidade_atendimento: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: 'tb_unidade_atendimento',
        key: 'id'
      }
    },
    no_equipe: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    nu_ine: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    st_ativo: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    ds_area: {
      type: DataTypes.STRING(20),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'tb_equipe',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "pk_equipe",
        unique: true,
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
};
