/* jshint indent: 2 */

const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  const tb_especialidade_corpo_clinico = sequelize.define('tb_especialidade_corpo_clinico', {
    id_corpo_clinico: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'tb_corpo_clinico',
        key: 'id'
      }
    },
    id_especialidade: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'tb_especialidade',
        key: 'id'
      }
    },
    id_profissao: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'tb_profissao',
        key: 'id'
      }
    },
    situacao: {
      type: DataTypes.CHAR(1),
      allowNull: true,
      defaultValue: "A"
    }
  }, {
    sequelize,
    tableName: 'tb_especialidade_corpo_clinico',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "tb_especialidade_corpo_clinico_fk",
        unique: true,
        fields: [
          { name: "id_corpo_clinico" },
          { name: "id_especialidade" },
        ]
      },
    ]
  });
      tb_especialidade_corpo_clinico.associate = function(models) {
          tb_especialidade_corpo_clinico.belongsTo(models.tb_profissao, {foreignKey: 'id_profissao'})
      };

  return tb_especialidade_corpo_clinico;
}
     
