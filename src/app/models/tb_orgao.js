/* jshint indent: 2 */

const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('tb_orgao', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    nome_orgao: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    sigla: {
      type: DataTypes.STRING(20),
      allowNull: false,
      unique: "tb_orgao_sigla_un"
    },
    logradouro: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    numero: {
      type: DataTypes.STRING(10),
      allowNull: true
    },
    bairro: {
      type: DataTypes.STRING(60),
      allowNull: true
    },
    complemento: {
      type: DataTypes.STRING(60),
      allowNull: true
    },
    cep: {
      type: DataTypes.STRING(9),
      allowNull: true
    },
    cidade: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    estado: {
      type: DataTypes.CHAR(2),
      allowNull: true
    },
    telefone: {
      type: DataTypes.STRING(15),
      allowNull: true
    },
    nome_responsavel: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    telefone_resp: {
      type: DataTypes.STRING(15),
      allowNull: true
    },
    situacao: {
      type: DataTypes.CHAR(1),
      allowNull: true,
      defaultValue: "A"
    },
    logomarca: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    id_usuario_incl: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    data_hora_incl: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: "(now"
    },
    id_usuario_altr: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    data_hora_altr: {
      type: DataTypes.DATE,
      allowNull: true
    },
    cod_ibge: {
      type: DataTypes.STRING(10),
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'tb_orgao',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "tb_orgao_id_pk",
        unique: true,
        fields: [
          { name: "id" },
        ]
      },
      {
        name: "tb_orgao_sigla_un",
        unique: true,
        fields: [
          { name: "sigla" },
        ]
      },
    ]
  });
};
