/* jshint indent: 2 */

const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('tb_prof_usuario', {
    id_usuario: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'tb_usuario',
        key: 'id'
      }
    },
    id_profissao: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'tb_profissao',
        key: 'id'
      }
    }
  }, {
    sequelize,
    tableName: 'tb_prof_usuario',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "tb_prof_usuario_pk",
        unique: true,
        fields: [
          { name: "id_usuario" },
          { name: "id_profissao" },
        ]
      },
    ]
  });
};
