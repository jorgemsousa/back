/* jshint indent: 2 */

const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('tb_cadastro_visita', {
    id: {
      type: DataTypes.UUID,
      allowNull: false,
      primaryKey: true
    },
    uuid: {
      type: DataTypes.STRING(44),
      allowNull: true
    },
    dirty: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
      defaultValue: false
    },
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'tb_usuario',
        key: 'id'
      }
    },
    familia_id: {
      type: DataTypes.UUID,
      allowNull: true
    },
    cadastro_cidadao_uuid: {
      type: DataTypes.STRING(44),
      allowNull: true
    },
    cadastro_domiciliar_uuid: {
      type: DataTypes.STRING(44),
      allowNull: true
    },
    motivos_visita: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    desfecho: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    date_created: {
      type: DataTypes.DATE,
      allowNull: true
    },
    date_updated: {
      type: DataTypes.DATE,
      allowNull: true
    },
    exportado: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    anotacao: {
      type: DataTypes.STRING(200),
      allowNull: true
    },
    necessita_visita_endemias: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    data_visita: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    turno: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    status_visita_compartilhada_outro_profissional: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    peso_acompanhamento_nutricional: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    altura_acompanhamento_nutricional: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    id_lote_prod_agente: {
      type: DataTypes.BIGINT,
      allowNull: true
    },
    unidade_atendimento_id: {
      type: DataTypes.BIGINT,
      allowNull: true,
      references: {
        model: 'tb_unidade_atendimento',
        key: 'id'
      }
    },
    familia_mudou: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    saida_cidadao_cadastro: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    cbo_profissional: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    equipe_id: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    problema_exportacao: {
      type: DataTypes.TEXT,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'tb_cadastro_visita',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "idx_cad_visita_id",
        fields: [
          { name: "id" },
        ]
      },
      {
        name: "tb_cadastro_visita_pk",
        unique: true,
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
};
