/* jshint indent: 2 */

const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('tb_profissao', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true
    },
    nome: {
      type: DataTypes.STRING(200),
      allowNull: false
    },
    situacao: {
      type: DataTypes.CHAR(1),
      allowNull: true,
      defaultValue: "A"
    },
    id_usuario_incl: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    data_hora_incl: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: "(now"
    },
    id_usuario_altr: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    data_hora_altr: {
      type: DataTypes.DATE,
      allowNull: true
    },
    cod_cbo: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    disponivel_unidade: {
      type: DataTypes.CHAR(1),
      allowNull: true
    },
    nome_filtro: {
      type: DataTypes.STRING(200),
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'tb_profissao',
    schema: 'public',
    timestamps: false,
  
  });
};
