/* jshint indent: 2 */

const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  const tb_usuario = sequelize.define('tb_usuario', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    nome: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    login: {
      type: DataTypes.STRING(100),
      allowNull: false,
      unique: "tb_usuario_login_un"
    },
    senha: {
      type: DataTypes.STRING(200),
      allowNull: true
    },
    email: {
      type: DataTypes.STRING(100),
      allowNull: false,
      unique: "tb_usuario_email_un"
    },
    nr_matricula: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    nr_cpf: {
      type: DataTypes.CHAR(11),
      allowNull: false,
      unique: "tb_usuario_cpf_un"
    },
    logradouro: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    numero: {
      type: DataTypes.STRING(10),
      allowNull: true
    },
    bairro: {
      type: DataTypes.STRING(60),
      allowNull: true
    },
    complemento: {
      type: DataTypes.STRING(60),
      allowNull: true
    },
    cep: {
      type: DataTypes.STRING(9),
      allowNull: true
    },
    cidade: {
      type: DataTypes.STRING(60),
      allowNull: true
    },
    estado: {
      type: DataTypes.CHAR(2),
      allowNull: true
    },
    telefone: {
      type: DataTypes.STRING(15),
      allowNull: true
    },
    situacao: {
      type: DataTypes.CHAR(1),
      allowNull: true,
      defaultValue: "A"
    },
    sexo: {
      type: DataTypes.CHAR(1),
      allowNull: false,
      defaultValue: "M"
    },
    autenticacao_pendente: {
      type: DataTypes.CHAR(1),
      allowNull: true,
      defaultValue: "S"
    },
    token_autenticacao: {
      type: DataTypes.STRING(24),
      allowNull: false
    },
    data_criacao_token: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: "NULL"
    },
    data_expiracao_token: {
      type: DataTypes.DATE,
      allowNull: false
    },
    token_validado: {
      type: DataTypes.CHAR(1),
      allowNull: true,
      defaultValue: "N"
    },
    data_criacao: {
      type: DataTypes.DATEONLY,
      allowNull: false,
      defaultValue: "(now"
    },
    id_usuario_incl: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    data_hora_incl: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: "(now"
    },
    id_usuario_altr: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    data_hora_altr: {
      type: DataTypes.DATE,
      allowNull: true
    },
    foto: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    dta_nascimento: {
      type: DataTypes.DATE,
      allowNull: true
    },
    nr_cns: {
      type: DataTypes.STRING(15),
      allowNull: true
    },
    conselho_class: {
      type: DataTypes.STRING(10),
      allowNull: true
    },
    uf_emissor: {
      type: DataTypes.CHAR(2),
      allowNull: true
    },
    nr_rcc: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    data_validade_senha: {
      type: DataTypes.DATE,
      allowNull: true
    },
    st_forcar_troca_senha: {
      type: DataTypes.CHAR(1),
      allowNull: true,
      defaultValue: "N"
    },
    senha_anterior: {
      type: DataTypes.STRING(200),
      allowNull: true
    },
    st_termo_uso: {
      type: DataTypes.CHAR(1),
      allowNull: true,
      defaultValue: "N"
    }
  }, {
    sequelize,
    tableName: 'tb_usuario',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "tb_usuario_cpf_un",
        unique: true,
        fields: [
          { name: "nr_cpf" },
        ]
      },
      {
        name: "tb_usuario_email_un",
        unique: true,
        fields: [
          { name: "email" },
        ]
      },
      {
        name: "tb_usuario_id_pk",
        unique: true,
        fields: [
          { name: "id" },
        ]
      },
      {
        name: "tb_usuario_login_un",
        unique: true,
        fields: [
          { name: "login" },
        ]
      },
    ]
  });

  tb_usuario.prototype.comparePassword = function (passw, cb) {
    
    var sp = this.senha.toString();
    var salt = sp.split(":")[1];
    var pw = passw + ":" + salt;
    
    if(pw != this.senha) {
      return cb(true);
    }
    return cb(null, true);
  };
  
  return tb_usuario;
};
