/* jshint indent: 2 */

const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('tb_prof_especialidade', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    id_profissao: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: 'tb_profissao',
        key: 'id'
      }
    },
    id_especialidade: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'tb_especialidade',
        key: 'id'
      }
    }
  }, {
    sequelize,
    tableName: 'tb_prof_especialidade',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "tb_prof_especialidade_id_pk",
        unique: true,
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
};
