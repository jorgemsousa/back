var DataTypes = require("sequelize").DataTypes;
var _tb_tipo_logradouro = require("./tb_tipo_logradouro");

function initModels(sequelize) {
  var tb_tipo_logradouro = _tb_tipo_logradouro(sequelize, DataTypes);


  return {
    tb_tipo_logradouro,
  };
}
module.exports = initModels;
module.exports.initModels = initModels;
module.exports.default = initModels;
