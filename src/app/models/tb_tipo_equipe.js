/* jshint indent: 2 */

const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('tb_tipo_equipe', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true
    },
    sg_tipo_equipe: {
      type: DataTypes.STRING(30),
      allowNull: false
    },
    no_tipo_equipe: {
      type: DataTypes.STRING(200),
      allowNull: false
    },
    nu_ms: {
      type: DataTypes.STRING(4),
      allowNull: false
    },
    ds_tp_equipe: {
      type: DataTypes.STRING(4),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'tb_tipo_equipe',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "pk_tipoequipe",
        unique: true,
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
};
