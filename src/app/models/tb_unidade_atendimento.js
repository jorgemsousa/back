/* jshint indent: 2 */

const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('tb_unidade_atendimento', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    nome: {
      type: DataTypes.STRING(60),
      allowNull: true
    },
    id_tipo_unidade_saude: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: 'tb_tipo_unidade_saude',
        key: 'id'
      }
    },
    id_orgao: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'tb_orgao',
        key: 'id'
      }
    },
    id_secretaria: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'tb_secretaria',
        key: 'id'
      }
    },
    logradouro: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    numero: {
      type: DataTypes.STRING(10),
      allowNull: true
    },
    bairro: {
      type: DataTypes.STRING(60),
      allowNull: true
    },
    complemento: {
      type: DataTypes.STRING(60),
      allowNull: true
    },
    cep: {
      type: DataTypes.STRING(9),
      allowNull: true
    },
    cidade: {
      type: DataTypes.STRING(60),
      allowNull: true
    },
    estado: {
      type: DataTypes.CHAR(2),
      allowNull: true
    },
    telefone: {
      type: DataTypes.STRING(15),
      allowNull: true
    },
    nome_responsavel: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    telefone_resp: {
      type: DataTypes.STRING(15),
      allowNull: true
    },
    email_resp: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    hora_atend_inicio: {
      type: DataTypes.DATE,
      allowNull: false
    },
    hora_atend_fim: {
      type: DataTypes.DATE,
      allowNull: false
    },
    hora_abertura: {
      type: DataTypes.DATE,
      allowNull: false
    },
    hora_fechamento: {
      type: DataTypes.DATE,
      allowNull: false
    },
    tempo_consulta_medica: {
      type: DataTypes.SMALLINT,
      allowNull: true,
      defaultValue: 30,
      comment: "Tempo mÃ©dio de consulta em minutos"
    },
    id_usuario_incl: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    data_hora_incl: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: "(now"
    },
    id_usuario_altr: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    data_hora_altr: {
      type: DataTypes.DATE,
      allowNull: true
    },
    situacao: {
      type: DataTypes.CHAR(1),
      allowNull: true,
      defaultValue: "A"
    },
    cod_cnes: {
      type: DataTypes.STRING(10),
      allowNull: false
    },
    cod_ine: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    nr_cnpj: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    in_hospitalar: {
      type: DataTypes.CHAR(1),
      allowNull: false,
      defaultValue: "N"
    }
  }, {
    sequelize,
    tableName: 'tb_unidade_atendimento',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "tb_unidatende_id_pk",
        unique: true,
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
};
