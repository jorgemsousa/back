/* jshint indent: 2 */

const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('tb_cadastro_familiar', {
    id: {
      type: DataTypes.UUID,
      allowNull: false,
      primaryKey: true
    },
    dirty: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
      defaultValue: false
    },
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    prontuario_familiar: {
      type: DataTypes.BIGINT,
      allowNull: true
    },
    rendafamiliar: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    reside_desde: {
      type: DataTypes.STRING(7),
      allowNull: true
    },
    familia_mudou: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
      defaultValue: false
    },
    date_created: {
      type: DataTypes.DATE,
      allowNull: true
    },
    date_updated: {
      type: DataTypes.DATE,
      allowNull: true
    },
    exportado: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    anotacao: {
      type: DataTypes.STRING(200),
      allowNull: true
    },
    cadastro_domicilio_uuid: {
      type: DataTypes.STRING(44),
      allowNull: true
    },
    unidade_atendimento_id: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    cbo_profissional: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    equipe_id: {
      type: DataTypes.INTEGER,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'tb_cadastro_familiar',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "tb_cadastro_familiar_pk",
        unique: true,
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
};
