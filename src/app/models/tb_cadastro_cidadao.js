/* jshint indent: 2 */

const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('tb_cadastro_cidadao', {
    id: {
      type: DataTypes.UUID,
      allowNull: false,
      primaryKey: true
    },
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    uuid: {
      type: DataTypes.STRING(44),
      allowNull: true
    },
    uuid_ficha_originadora: {
      type: DataTypes.STRING(44),
      allowNull: true
    },
    inactive: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
      defaultValue: false
    },
    dirty: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
      defaultValue: false
    },
    data_cadastro: {
      type: DataTypes.DATE,
      allowNull: false
    },
    familia_id: {
      type: DataTypes.UUID,
      allowNull: false
    },
    cns_cidadao: {
      type: DataTypes.BIGINT,
      allowNull: true
    },
    status_responsavel: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    cns_responsavel_familiar: {
      type: DataTypes.BIGINT,
      allowNull: true
    },
    microarea: {
      type: DataTypes.STRING(2),
      allowNull: true
    },
    st_fora_area: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    nome_cidadao: {
      type: DataTypes.STRING(70),
      allowNull: true
    },
    nome_social: {
      type: DataTypes.STRING(70),
      allowNull: true
    },
    data_nascimento: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    sexo: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    raca_cor: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    etnia: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    nr_nis: {
      type: DataTypes.STRING(11),
      allowNull: true
    },
    nome_mae: {
      type: DataTypes.STRING(70),
      allowNull: true
    },
    mae_desconhecida: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    nome_pai: {
      type: DataTypes.STRING(70),
      allowNull: true
    },
    pai_desconhecido: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    nacionalidade: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    pais_nascimento: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    portaria_naturalizacao: {
      type: DataTypes.STRING(16),
      allowNull: true
    },
    dta_naturalizacao: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    dta_entrada_brasil: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    cod_ibge_munic_nascimento: {
      type: DataTypes.STRING(7),
      allowNull: true
    },
    status_termo_recusa_cad_individual_atencao_basica: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    telefone_celular: {
      type: DataTypes.STRING(11),
      allowNull: true
    },
    email: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    relacao_parentesco: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    ocupacao_cod_cbo2002: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    status_frequenta_escola: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    grau_instrucao: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    situacao_mercado_trabalho: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    status_membro_povo_comunidade_tradicional: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    status_frequenta_benzedeira: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    status_participa_grupo_comunitario: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    povo_comunidade_tradicional: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    status_possui_plano_saude_privado: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    status_deseja_informar_orientacao_sexual: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    orientacao_sexual: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    status_deseja_informar_ident_genero: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    identidade_genero: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    status_tem_alguma_deficiencia: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    saida_cadastro: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    motivo_saida: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    status_gestante: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    maternidade_referencia: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    situacao_peso: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    status_fumante: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    status_dependente_alcool: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    status_dependente_outras_drogas: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    status_tem_hipertensao_arterial: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    status_tem_diabetes: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    status_teve_avc_derrame: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    status_teve_infarto: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    status_teve_doenca_cardiaca: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    status_tem_teve_doencas_rins: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    status_tem_doenca_respiratoria: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    status_tem_hanseniase: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    status_tem_tuberculose: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    status_tem_teve_cancer: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    status_teve_internadoem_12meses: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    desc_causa_internacao_12meses: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    status_diagnostico_mental: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    status_acamado: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    status_domiciliado: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    status_usa_plantas_medicinais: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    desc_plantas_medicinais_usadas: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    status_usa_outras_praticas_integrativas_complem: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    desc_outra_condicao1: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    desc_outra_condicao2: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    desc_outra_condicao3: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    status_situacao_rua: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    tempo_situacao_rua: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    status_recebe_ceneficio: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    status_possui_referencia_familiar: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    quant_refeicooes_diaria_rua: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    status_acompanhado_por_outra_instituicao: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    outra_instituicao_que_acompanha: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    status_visita_familiar_frequentemente: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    grau_parentesco_familiar_frequentado: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    status_tem_acesso_higiene_pessoal_situacao_rua: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    anotacao: {
      type: DataTypes.STRING(200),
      allowNull: true
    },
    st_exportado: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    ficha_atualizada: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    date_created: {
      type: DataTypes.DATE,
      allowNull: true
    },
    date_updated: {
      type: DataTypes.DATE,
      allowNull: true
    },
    nr_prontuario_ubs: {
      type: DataTypes.BIGINT,
      allowNull: true
    },
    cpf: {
      type: DataTypes.STRING(11),
      allowNull: true
    },
    rg: {
      type: DataTypes.STRING(11),
      allowNull: true
    },
    titulo_eleitor: {
      type: DataTypes.STRING(12),
      allowNull: true
    },
    situacao_conjugal: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    funcionario_publico: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    status_recebe_bolsa_familia: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    alfabetizado: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    deficiencias: {
      type: DataTypes.STRING(25),
      allowNull: true
    },
    peso: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    altura: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    status_desnutricao_grave: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    status_tem_diarreia: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    status_tem_cancer: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    tipo_tem_cancer: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    status_teve_cancer: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    tipo_teve_cancer: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    tipo_diabetes: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    doenca_cardiaca: {
      type: DataTypes.STRING(25),
      allowNull: true
    },
    doenca_rins: {
      type: DataTypes.STRING(25),
      allowNull: true
    },
    doenca_respiratoria: {
      type: DataTypes.STRING(25),
      allowNull: true
    },
    status_pratica_atividades_fisicas: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    origem_alimento_situacao_rua: {
      type: DataTypes.STRING(25),
      allowNull: true
    },
    higiene_pessoal_situacao_rua: {
      type: DataTypes.STRING(25),
      allowNull: true
    },
    numero_do: {
      type: DataTypes.STRING(9),
      allowNull: true
    },
    data_obito: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    responsavel_por_crianca: {
      type: DataTypes.STRING(25),
      allowNull: true
    },
    unidade_atendimento_id: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    familia_mudou: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    cbo_profissional: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    equipe_id: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    id_lote_prod_agente: {
      type: DataTypes.BIGINT,
      allowNull: true
    },
    problema_exportacao: {
      type: DataTypes.TEXT,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'tb_cadastro_cidadao',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "cidadao_uuid_familia_idx",
        fields: [
          { name: "familia_id" },
        ]
      },
      {
        name: "cidadao_uuid_ficha_originadora_idx",
        fields: [
          { name: "uuid_ficha_originadora" },
        ]
      },
      {
        name: "tb_cadastro_cidadao_pk",
        unique: true,
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
};
