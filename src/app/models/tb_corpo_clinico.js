/* jshint indent: 2 */

const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  const tb_corpo_clinico = sequelize.define('tb_corpo_clinico', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    id_unidade_atendimento: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'tb_unidade_atendimento',
        key: 'id'
      },
      unique: "tb_corpo_clinico_un"
    },
    id_pessoa: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'tb_usuario',
        key: 'id'
      },
      unique: "tb_corpo_clinico_un"
    },
    id_equipe: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: 'tb_equipe',
        key: 'id'
      }
    },
    situacao: {
      type: DataTypes.CHAR(1),
      allowNull: true
    },
    data_inicio: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: "(now"
    },
    data_fim: {
      type: DataTypes.DATE,
      allowNull: true
    },
    id_usuario_incl: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    data_hora_incl: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: "(now"
    },
    id_usuario_altr: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    data_hora_altr: {
      type: DataTypes.DATE,
      allowNull: true
    },
    id_profissao: {
      type: DataTypes.INTEGER,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'tb_corpo_clinico',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "tb_corpo_clinico_pk",
        unique: true,
        fields: [
          { name: "id" },
        ]
      },
      {
        name: "tb_corpo_clinico_un",
        unique: true,
        fields: [
          { name: "id_unidade_atendimento" },
          { name: "id_pessoa" },
        ]
      },
    ]
  });

    tb_corpo_clinico.associate = function(models) {
      tb_corpo_clinico.belongsTo(models.tb_usuario, {foreignKey: 'id_pessoa'})
      tb_corpo_clinico.belongsTo(models.tb_unidade_atendimento, {foreignKey: 'id_unidade_atendimento'})
    };

  return tb_corpo_clinico
};
