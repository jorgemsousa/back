const { Router } = require('express');
const CidadaoCadController = require('./app/controllers/CidadaoCadController');
const DomicilioCadController = require('./app/controllers/DomicilioCadController');
const FamiliaCadController = require('./app/controllers/FamiliaCadController')
const VsitaCadController = require('./app/controllers/VisitaCadController')

const CidadaoController = require('./app/controllers/CidadaoIncController');
const DomicilioController = require('./app/controllers/DomicilioIncController');

const UserController = require('./app/controllers/UserContrller');
const AuthController = require('./app/controllers/AuthController');

const authMiddleware = require('./app/middleware/auth');
const VisitaCadController = require('./app/controllers/VisitaCadController');

const UnityAttendance = require('./app/controllers/UnityController');
const TeamAttendance = require('./app/controllers/TeamController');
const ClinicalStaff = require('./app/controllers/ClinicalStaffController');
const TipoLogradouroController = require('./app/controllers/TipoLogradouroController');

const routes = Router();    

//rota de autenticação
routes.post('/authenticate', AuthController.index);
routes.post('/getagent', AuthController.show);

routes.use(authMiddleware);

// rotas de inconsistencias
routes.get('/cidadao', CidadaoController.index);
routes.get('/domicilio', DomicilioController.index);

// rotas de cadastros
routes.get('/cidadaocad', CidadaoCadController.index);
routes.get('/cidadaos', CidadaoCadController.showAll);
routes.get('/cidadaos/familias', CidadaoCadController.showResponsavel);
routes.get('/domiciliocad', DomicilioCadController.index);
routes.get('/domicilios', DomicilioCadController.showAll  );
routes.get('/familiacad', FamiliaCadController.index);
routes.get('/familias', FamiliaCadController.showAll);
routes.get('/visitacad', VsitaCadController.index)   

routes.post('/domiciliocad', DomicilioCadController.create)
routes.post('/familiacad', FamiliaCadController.create);
routes.post('/visitacad', VsitaCadController.create)   

//rotas listar um em cadastro
routes.get('/cidadaoshow/cadast', CidadaoCadController.show)
routes.get('/domicilioshow/cadast/:_id', DomicilioCadController.show)
routes.get('/familiashow', FamiliaCadController.show)
routes.get('/visitashow/cadast/:_id', VisitaCadController.show)

//rotas listar um inconsistencias
routes.get('/cidadaoshow/inconsist/:_id', CidadaoController.show)
routes.get('/domicilioshow/inconsist/:_id', DomicilioController.show)

//rotas unidades atendimento
routes.get('/unity', UnityAttendance.index)

//rotas de equipe
routes.get('/teams', TeamAttendance.index)

//rotas corpoclinico
routes.get('/clinicalStaff', ClinicalStaff.index)

//rotas de users
routes.get('/users', UserController.index);
routes.get('/users/user/:_id', UserController.show);

//rotas tipo logradouros
routes.get('/tipologradouro', TipoLogradouroController.index)

module.exports = routes;