const { tb_cadastro_familiar, tb_cadastro_domiciliar, sequelize } = require('../models');
const { QueryTypes } = require('sequelize')
const { v4 : uuidv4 } = require('uuid');


module.exports = {

    async index(req, res) {
        const { id } = req.query
        let {limit = 10 } = req.query
        let { page = 1 } = req.query
        page = parseInt(page - 1)
        limit = parseInt(limit)

        try {
            const {count: size, rows: familias} = await tb_cadastro_familiar.findAndCountAll({                
                limit: limit,
                offset: page * limit,
                where: {
                    user_id: id
                }          
            })
            return res.json({size, familias})
            
        } catch (error) {
            return res.status(400).send({ error: 'Error list familys'})
        }        
    },

    async showAll(req, res) {
        const { id } = req.query
        
        try {
            const {count: size, rows: familias} = await tb_cadastro_familiar.findAndCountAll({                
                where: {
                    cadastro_domicilio_uuid: id,
                    familia_mudou: false,
                }                
            })
            return res.json({size, familias})
            
        } catch (error) {
            return res.status(400).send({ error: 'Error list familys'})
        }        
    },

    async create(req, res){
          const user = req.body.user
          const data = req.body.data
          const date = new Date();

          try {
            const familia = await tb_cadastro_familiar.create({
                id: uuidv4(),
                dirty: false,
                user_id: user.id,
                prontuario_familiar: data.prontuario_familiar || '',
                rendafamiliar: data.rendafamiliar || null,
                reside_desde: data.reside_desde || '',
                familia_mudou: data.familia_mudou,
                anotacao: data.anotacao || '',
                cadastro_domicilio_uuid: data.cadastro_domicilio_uuid || '',
                unidade_atendimento_id: user.unidade_atendimento_id || null,
                cbo_profissional: user.cbo_profissional || '',
                equipe_id: user.equipe_id || null,
                date_created: date
             })

            return res.send(familia)
            
        } catch (error) {
            return res.status(400).send({ error: 'Error create a new family'})
        }
    },

    async show(req, res) {
        const { id, prontuario } = req.query
        try {
            const familia = await sequelize.query(
                `SELECT DISTINCT * FROM tb_cadastro_familiar as familia INNER JOIN tb_cadastro_domiciliar as domicilio
                    ON familia.cadastro_domicilio_uuid=domicilio.uuid_ficha_originadora AND familia.user_id=${id} AND familia.prontuario_familiar = ${prontuario}`, { type: QueryTypes.SELECT }
            ) 
            return res.json({familia})
            
        } catch (error) {
            return res.status(400).send({ error: 'Error show family'})
        }
    },

    async update(req, res){
        const family = req.body;
        console.log(family)
        try {
            const familia = await tb_cadastro_familiar.findOne({ where: { id : req.params._id } })

            return res.send(familia)
            
        } catch (error) {
            return res.status(400).send({ error: 'Error update family'})
        }
    },

    async delete(req, res) {
        try {
            await tb_cadastro_familiar.destroy({ where: { id : req.params._id } })

            return res.send()
            
        } catch (error) {
            return res.status(400).send({ error: 'Error delete family'})
        }
    }
}